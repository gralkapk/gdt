// ======================================================================== //
// Copyright 2018 Ingo Wald                                                 //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "Camera.h"

namespace gdt {
  namespace viewer {

        // ------------------------------------------------------------------
    /*! camera manipulator with the following traits
      
      - there is a "point of interest" (POI) that the camera rotates
      around.  (we track this via poiDistance, the point is then
      thta distance along the fame's z axis)
      
      - we can restrict the minimum and maximum distance camera can be
      away from this point
      
      - we can specify a max bounding box that this poi can never
      exceed (validPoiBounds).
      
      - we can restrict whether that point can be moved (by using a
      single-point valid poi bounds box
      
      - left drag rotates around the object

      - right drag moved forward, backward (within min/max distance
      bounds)

      - middle drag strafes left/right/up/down (within valid poi
      bounds)
      
    */
    struct InspectModeManip : public FullCameraManip {

      InspectModeManip(ViewerWidget *widget,
                       const box3f &validPoiBounds,
                       const float minDistance,
                       const float maxDistance)
        : FullCameraManip(widget),
          validPoiBounds(validPoiBounds),
          minDistance(minDistance),
          maxDistance(maxDistance)
      {}

      /*! this gets called when the user presses a key on the keyboard ... */
      virtual void key(char key, const vec2i &where) override;
      /*! this gets called when the user presses a key on the keyboard ... */
      virtual void special(int key, const vec2i &where) override;
      
    private:
      /*! helper function: rotate camera frame by given degrees, then
          make sure the frame, poidistance etc are all properly set,
          the widget gets notified, etc */
      void rotate(const float deg_x, const float deg_y);
      
      /*! helper function: move forward/backwards by given multiple of
          motion speed, then make sure the frame, poidistance etc are
          all properly set, the widget gets notified, etc */
      void move(const float step);

      /*! move the POI itself */
      void movePOI(const vec2f delta);

      void kbd_up();
      void kbd_down();
      void kbd_left();
      void kbd_right();
      void kbd_forward();
      void kbd_back();
      
      /*! mouse got dragged with left button pressedn, by 'delta'
          pixels, at last position where */
      virtual void mouseDragLeft  (const vec2i &where, const vec2i &delta) override;

      /*! mouse got dragged with left button pressedn, by 'delta'
          pixels, at last position where */
      virtual void mouseDragRight (const vec2i &where, const vec2i &delta) override;

      /*! mouse got dragged with left button pressedn, by 'delta'
          pixels, at last position where */
      virtual void mouseDragCenter(const vec2i &where, const vec2i &delta) override;
      

      /*! if non-empty, bounding box of region the poi is not allowed
        to leave */
      const box3f validPoiBounds;
      const float minDistance;
      const float maxDistance;
    };


  } // ::gdt::viewer
} // ::gdt
